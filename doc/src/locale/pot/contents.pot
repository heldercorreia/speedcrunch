# SOME DESCRIPTIVE TITLE.
# Copyright (C) 2016, The SpeedCrunch Developers
# This file is distributed under the same license as the SpeedCrunch package.
# FIRST AUTHOR <EMAIL@ADDRESS>, YEAR.
#
#, fuzzy
msgid ""
msgstr ""
"Project-Id-Version: SpeedCrunch master\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2022-12-23 20:56+0100\n"
"PO-Revision-Date: YEAR-MO-DA HO:MI+ZONE\n"
"Last-Translator: FULL NAME <EMAIL@ADDRESS>\n"
"Language-Team: LANGUAGE <LL@li.org>\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"

#: ../../contents.rst:2
msgid "Table of Contents"
msgstr ""

#: ../../contents.rst:14
msgid "Indices and Tables"
msgstr ""

#: ../../contents.rst:21
#: ../../contents.rst:26
msgid ":ref:`genindex`"
msgstr ""

#: ../../contents.rst:22
#: ../../contents.rst:27
msgid ":ref:`sc:functionindex`"
msgstr ""

#: ../../contents.rst:28
msgid ":ref:`search`"
msgstr ""
